resource "aws_route53_zone" "teste"{
    name = "desafio03thiago.tk"

}

resource "aws_route53_record" "blog" {
zone_id = aws_route53_zone.teste.id
type = "A"
name = "blog"
ttl = 300
records = [var.ip]

}

resource "aws_route53_record" "loja-blog" {
zone_id = aws_route53_zone.teste.id
type = "A"
name = "loja-blog"
ttl = 300
records = [var.ip]

}

resource "aws_route53_record" "tomcat" {
zone_id = aws_route53_zone.teste.id
type = "A"
name = "tomcat"
ttl = 300
records = [var.ip]

}

resource "aws_route53_record" "estatico" {
zone_id = aws_route53_zone.teste.id
type = "CNAME"
name = "www"
ttl = 300
records = [var.ip]

}