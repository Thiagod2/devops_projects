provider "aws"{
   region = "us-east-1"
}

 resource "aws_key_pair" "thiago" {
     key_name = var.key
     public_key = var.public
 }  

resource "aws_instance" "teste" {
    ami = var.ami_id
    instance_type = var.instance
    vpc_security_group_ids = [aws_security_group.allow_http_https.id]
    #security_groups = teste_123
    subnet_id = var.sub_id
    associate_public_ip_address = true
    key_name = var.key
    #public_ip = var.ip
 
    tags = {
        Name = "Teste_Dev_Thiago"
        owner = "Thiago Brito"
        team = "pulmão"
    }


}

resource "aws_security_group" "allow_http_https" {
    name = "teste_123"
    vpc_id = var.aws_vpc
    description = "Terraform Thiago"
    #id = var.security_id


    ingress {
        
      from_port = var.from_http
      to_port = var.to_http
      protocol = var.protocol_id
      cidr_blocks = ["0.0.0.0/0"]  

     }
     
    
    ingress {

      from_port = var.from_https
      to_port = var.to_https
      protocol = var.protocol_id
      cidr_blocks = ["0.0.0.0/0"]     
    
    }

    ingress {

      from_port = var.ssh
      to_port = var.ssh
      protocol = var.protocol_id
      cidr_blocks = ["0.0.0.0/0"]     
    
    }


    ingress {
        from_port = var.from_tom
        to_port = var.to_tom
        protocol = var.protocol_id
        cidr_blocks = ["0.0.0.0/0"]  
    }
     
      

    egress {
        from_port = var.port_egress
        to_port = var.to_port_egress
        protocol = var.protocol_egress
        cidr_blocks = ["0.0.0.0/0"]  
    }

    
     tags = {
        #team = "Pulmão"
        Name = "Teste_Security"
        #owner = "Thiago_Brito"
    }
}

 output "public" {
        value = aws_instance.teste.public_ip
    }
