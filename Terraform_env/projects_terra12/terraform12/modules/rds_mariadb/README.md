# Estrutura do Projeto
```
.
├── main.tf
├── outputs.tf
├── README.md
└── variables.tf

0 directories, 4 files

```
# Código e sua Funcionalidade
Como solicitado no desafio 10 a criação de um Banco mysql com usuarios diferentes e tipos de acessos diferentes tambem,criei um banco RDS com a engine MariaDB na AWS via Terraform,dentro do repositorio possui o código main com as configurações do banco como tipo de instancia utilizada para cria-lo e as proprias configurações de usuario mesmo.

# Dados Sensiveis 
O código variables com as senhas estão na minha maquina local,subi o arquivo variables com nenhum valor declarado nelas caso seja solicitado posso subir encryptado.

# Referencias
[Terraform RDS](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/db_instance)