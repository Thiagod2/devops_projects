variable "region" {
    description = "Location of the cluster."
    default = "us-east-1"
}

variable "vpc_id"  {
    description = "ID of the VPC to create the cluster"
    default = ""
    type = string
}

variable "subnet_ids" {
  type    = list(string)
  default = [
    "",
    ""
  ]
}